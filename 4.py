# coding: utf-8


class Node:
    """
    Элемент односвязного списка
    """

    def __init__(self, value=None, next=None):
        self.value = value
        self.next = next


class LinkedList:
    """
    Односвязный список
    """

    def __init__(self):
        self.first = None
        self.last = None
        self.length = 0

    def __str__(self):
        if self.first:
            current = self.first
            out = 'LinkedList [' + str(current.value)
            while current.next:
                current = current.next
                out += ', ' + str(current.value)
            return out + ']'
        return 'LinkedList []'

    def clear(self):
        self.__init__()

    def add(self, x):
        self.length += 1
        if not self.first:
            self.last = self.first = Node(x, None)
        else:
            self.last.next = self.last = Node(x, None)


def generate_LinkedList(number_of_elements):
    """
    Метод для генерации односвязного списка длиной n.

    :param number_of_elements: длина односвязного списка
    :return: объект LinkedList
    """

    linked_list = LinkedList()

    # Сложность ~O(n)
    for number in range(0, number_of_elements):
        linked_list.add(number)

    return linked_list


def make_massive_from_LinkedList(linked_list):
    """
    Метод для создания "массива" на основе имеющегося односвязного списка

    :param linked_list: односвязный список
    :return: "массив"
    """

    # Питоний список не совсем массив, но для создания прототипа подойдет.
    # Считаем что это массив
    massive = []

    if linked_list.first:

        # Сложность ~O(n)
        current = linked_list.first
        massive.append(current.value)
        while current.next:
            current = current.next
            massive.append(current.value)

    return massive


def make_special_sorted_LinkedList_from_massive(massive):
    """
    Метод для сортировки "массива" и последующей записи в односвязный список

    :param massive: "массив" из которого сделаем
                    сортированный односвязный список
    :return: объект LinkedList
    """

    special_sorted_linked_list = LinkedList()

    # Пусть мы не знаем длину "массива",
    # сложность ~O(n) на поиск длины "массива"
    massive_len = len(massive)
    half_massive_len = int(massive_len / 2)

    # Сложность ~O(n)
    for i in range(0, half_massive_len):
        special_sorted_linked_list.add(massive[i])
        special_sorted_linked_list.add(massive[-(i + 1)])

    if massive_len % 2 != 0:
        special_sorted_linked_list.add(massive[half_massive_len])

    return special_sorted_linked_list


if __name__ == '__main__':
    # Количество элементов в односвязном списке
    number_of_elements = 10

    # Создадим односвязный список ~O(n)
    linked_list = generate_LinkedList(number_of_elements)
    print(linked_list)

    # Сделаем "массив" из односвязного списка ~O(n)
    massive = make_massive_from_LinkedList(linked_list)

    # Сделаем из массива отсортированный односвязный список ~O(2n)
    special_sorted_linked_list = make_special_sorted_LinkedList_from_massive(
        massive
    )
    print(special_sorted_linked_list)

    # Итого общую сложность алгоритма оцениваю в ~O(4n)
