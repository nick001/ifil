# coding: utf-8

def divide(n, m):
    # Выходной список интервалов
    intervals = []
    # Количество элементов в каждой части
    el_count = int(n / m)
    # Количество элементов вектора, не попадающих в части
    rest_part = n % m
    
    # Отступ от начала вектора
    divide_start = int(rest_part / 2)
    # Отступ от конца вектора
    divide_end = int(n - (rest_part - rest_part / 2))

    for i in range(divide_start, divide_end, el_count):
        intervals.append([i, i + (el_count-1)])

    return intervals


if __name__ == '__main__':
    n = int(input('Enter vector size N: '))
    m = int(input('Enter number of parts M: '))

    if m >= n:
        raise ValueError('M must be less then N!')

    parts = divide(n, m)
    print(parts)
