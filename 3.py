# coding: utf-8

import sys

# Объявляю возможные типы опций (со значением или булевы)
TYPE_VALUE = 'value'
TYPE_BOOLEAN = 'boolean'

# Объявляю список доступных опций.
# Каждая опция это словарь:
# 'name': полное имя опции (с '--' вначале)
# 'type': тип опции (со значением или булев)
# 'description': описании опции
# 'required': обязательная опция?
POSSIBLE_OPTIONS = [
    {
        'name': '--all',
        'type': TYPE_VALUE,
        'description': 'ALL DESCRIPTION',
        'required': True
    }, {
        'name': '--inversed',
        'type': TYPE_BOOLEAN,
        'description': 'INVERSED DESCRIPTION',
        'required': False
    }
]


def main(args):
    """
    Парсер аргументов командной строки
    """

    # Получим список опций (список словарей) и аргументов (просто список),
    # из того что ввел пользователь в командной строке,
    # с учетом разрешенных (POSSIBLE_OPTIONS) опций
    options, arguments = get_options_and_arguments_from_args(args)

    # Если среди опций есть help, то закончим выполнение.
    # Сообщение с помощью генерируется ранее
    if options == 'help':
        return

    check_required_options(options)

    # Покажем аргументы какие указал пользователь в командной строке
    print('\nARGUMENTS:')
    for argument in arguments:
        print('    {}'.format(argument))

    # Покажем опции и значения которые указал пользователь в командной строке
    print('\nOPTIONS:')
    print('    {:>10}    {}'.format('NAME', 'VALUE'))
    for option in options:
        print('    {:>10}    {}'.format(
            option['option_name'], option['option_value']
        ))


def check_required_options(options):
    """
    Метод для проверки все ли обязательные опции указал пользователь.
    Выводит сообщение об ошибке и прекращает работу программы если
    какая-то из обязательных опций не указана

    :param options: опции которые ввел пользователь
    """

    # Проходим по всем возможным опциям
    for option in POSSIBLE_OPTIONS:

        # Если опция обязательная - то проверим указал ли её пользователь
        if option['required']:
            required_flag = False
            for input_option in options:
                if input_option['option_name'] == option['name']:
                    required_flag = True
                    break

            # Выведем сообщение с именем обязательной опции,
            # которую не указал пользователь и прекратим работу программмы
            if not required_flag:
                print('ERROR havent required option')
                print('{} - is not defined!!'.format(option['name']))
                sys.exit(2)


def get_options_and_arguments_from_args(args):
    """
    Метод для получения опций (список словарей) и аргументов (список)
    которые ввел пользователь при запуске скрипта

    :param args: аргументы введенные пользователем начиная с первого
                    (название файла со скриптом не будем проверять)
    :return: options, arguments - обработанные значения опций и аргументов
    """

    # Объявление переменных
    options = []
    arguments = []
    args_iter = iter(range(0, len(args)))

    # Если пользователь не указал никакие опции и аргументы, покажем хелп
    if len(args) == 0:
        help_message = generate_help_message()
        print(help_message)
        return 'help', None

    # Итерируемся по индексам аргументов командной строки
    for arg_index in args_iter:
        arg = args[arg_index]

        # Если одна из опций хелп, то
        # покажем сообщение с помощью и прекратим выполнение
        if arg == '--help':
            help_message = generate_help_message()
            print(help_message)
            return 'help', None

        # Если аргумент командной строки начинается с '--',
        # значит это опция
        if arg.startswith('--'):

            # Если в строке есть '=' значит пользователь ввел опцию вида:
            # название_опции=значение_опции
            if '=' in arg:

                # Получим имя опции из строки
                option_name = arg.split('=')[0]

                # Проверим есть ли данная опция в списке разрешенных,
                # и является ли её тип не булевым
                for option in POSSIBLE_OPTIONS:

                    # имя опции разрешенной опции
                    # такое как указал пользователь?
                    if option_name == option['name']:

                        # тип разрешенной опции не булев?
                        if option['type'] == TYPE_VALUE:
                            option_value = arg.split('=')[1]
                            break

                        # если тип опции булев, значит '=' лишнее
                        # и пользователь ошибся при вводе
                        elif option['type'] == TYPE_BOOLEAN:
                            print('ERROR with option with =.')
                            print('boolean and = in one line')
                            sys.exit(2)

                # если пользователь указал недопустимое
                # имя опции или пустое значение, то сообщим об ошибке
                if not option_value:
                    print('ERROR with option with =.')
                    print('option not possible or empty option value')
                    sys.exit(2)

            # если пользователь ввел опцию вида:
            # 'название_опции значение_опции' или булеву опцию
            else:

                # текущий аргумент командной строки будет названием опции
                option_name = arg

                # Проверим есть ли данная опция в списке разрешенных
                for option in POSSIBLE_OPTIONS:

                    # имя опции разрешенной опции
                    # такое как указал пользователь?
                    if option_name == option['name']:

                        # Значит пользователь ввел опцию вида:
                        # 'название_опции значение_опции', то
                        # следующий аргумент командной строки будет
                        # значением опции
                        if option['type'] == TYPE_VALUE:
                            arg_index += 1
                            option_value = args[arg_index]
                            next(args_iter, None)
                            break

                        # Значит пользователь ввел опцию типа булева.
                        # Поставим ей значение тру
                        elif option['type'] == TYPE_BOOLEAN:
                            option_value = True
                            break

                # если пользователь указал недопустимое
                # имя опции или пустое значение, то сообщим об ошибке
                if not option_value:
                    print('ERROR with option without =.')
                    print('option not possible or empty option value')
                    sys.exit(2)

            # создадим словарь с именем и значением текущей опции
            option = {
                'option_name': option_name,
                'option_value': option_value
            }
            options.append(option)

        # Если аргумент командной строки не начинается с '--',
        # значит это аргумент для программы. Так и запишем
        else:
            arguments.append(arg)

    return options, arguments


def generate_help_message():
    """
    Метод для генерации сообщения при указаннии опции help.
    Сообщение генерируется на основе разрешенных (POSSIBLE_OPTIONS) опций

    :return: отформатированное сообщение с помощью
    """

    # Создадим первую часть сообщения
    # с описанием работы программы и примером использования
    help_message = make_initial_help_examlpe()

    with_value_message = ''
    boolean_message = ''

    # Создадим вторую часть сообщения с описанием всех опций
    # Сперва нарисуем заголовки колонок для опций
    help_message += 'OPTIONS:\n'
    help_message += '{:>10} {:>8}  {}\n'.format('NAME', 'TYPE', 'DESCRIPTION')

    # Добавим информацию об опциях
    for value in POSSIBLE_OPTIONS:
        with_value_message += '{:>10} {:>8}  {}\n'.format(
            value['name'], value['type'], value['description'],
        )

    # Общее сообщение содержит описание программы,
    # пример использования программы и список всех опций
    help_message += with_value_message + boolean_message

    return help_message


def make_initial_help_examlpe():
    """
    Метод для генерации описания работы программы и
    генерации примера использования программы

    :return: строка с описанием и примером использования программы
    """

    # Создадим краткое описание программмы
    help_message = 'INITIAL HELP MESSAGE\nWITH PROGRAM README THERE?\n'

    # Создадим пример использования программы
    help_message += '{}\n'.format(usage_example())

    return help_message


def usage_example():
    """
    Метод для генерации примера использования программы,
    основан на допустимых (POSSIBLE_OPTIONS) опциях

    :return: строка с примером запуска программы
    """

    # Получим имя файла программы (текущее имя 3.py) и
    # объявим пустую строку для последующего заполнения опциями
    module_name = sys.argv[0]
    all_options_usage = ''

    # Пройдемся по всем опциям в списке
    for option in POSSIBLE_OPTIONS:

        # Сгенерируем часть сообщения для текущей опции (option)
        # в зависимости от типа опции (значение или буева опция)
        option_usage_by_type = '{}'.format(option['name'])
        if option['type'] == TYPE_VALUE:
            option_usage_by_type += '=<option_value>'

        # Если опция необязательна, то напишем её в квадратных скобочках
        if option['required']:
            all_options_usage += '{} '.format(option_usage_by_type)
        else:
            all_options_usage += '[{}] '.format(option_usage_by_type)

    return '{} {}'.format(module_name, all_options_usage)


if __name__ == '__main__':
   main(sys.argv[1:])
