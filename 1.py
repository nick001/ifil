# coding: utf-8

def show_me_prime_multipliers_for(N_max):
    """
    Показывает число и его простые множители
    для всех чисел от 1 до N_max включительно

    :param N_max: находим простые множители в интервале от 1 до N_max
    """

    if N_max < 1:
        print('N_max must be equal or greater than 1')
        return

    list_of_prime_numbers = make_list_of_prime_numbers(N_max)

    for number in range(1, N_max + 1):
        all_prime_multipliers_of_number = find_prime_multipliers(
            number, list_of_prime_numbers
        )
        print('NUMBER: {}\nALL NUMBER MULTIPLIERS: {}'.format(
            number, all_prime_multipliers_of_number
        ))


def make_list_of_prime_numbers(N_max):
    """
    Делает список простых чисел до N_max с помощью решета

    :param N_max: максимальное число для проверки на простоту
    :return: список простых чисел
    """
    list_of_all_numbers = list(range(0, N_max + 1))
    list_of_prime_numbers = []
    i = 2

    while i <= N_max:

        if list_of_all_numbers[i] != 0:
            list_of_prime_numbers.append(list_of_all_numbers[i])
            for j in range(i, N_max + 1, i):
                list_of_all_numbers[j] = 0

        i += 1

    return list_of_prime_numbers


def find_prime_multipliers(number, list_of_prime_numbers):
    """
    Формирует список простых множителей для number

    :param number: число для которого хотим найти простые множители
    :param list_of_prime_numbers: список простых чисел
    :return: список простых множителей
    """

    all_number_multiplicators = []
    flag = True

    while flag:

        for prime_number in list_of_prime_numbers:
            if number % prime_number == 0:
                number /= prime_number
                all_number_multiplicators.append(prime_number)
                break

        if number == 1:
            flag = False
            break

    return all_number_multiplicators


if __name__ == '__main__':
    # Верхняя граница интервала чисел
    # для которых будем находить простые множители
    N_max = 100000

    show_me_prime_multipliers_for(N_max)
